﻿using System.Threading.Tasks;
using Grpc.Net.Client;
using GrpcGreeter;
// using GrpcGreeterClient;
var handler = new HttpClientHandler
{
    ServerCertificateCustomValidationCallback =
    HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
};
// AppContext.SetSwitch(
//     "System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
// The port number must match the port of the gRPC server.
using var channel = GrpcChannel.ForAddress("https://localhost:5001", new GrpcChannelOptions { HttpHandler = handler });
var client = new Greeter.GreeterClient(channel);
var client2 = new MessageSrv.MessageSrvClient(channel);

var reply = await client.SayHelloAsync(
                  new HelloRequest { Name = "GreeterClient" });

Console.WriteLine("Greeting: " + reply.Message);

var reply3 = client2.RecordMessage(new MsgRequest() {
    Username = "Ben",
    Message = "Data Baru super lagi " + (new Random()).NextInt64()
});

var reply2 = client2.ListMessage(new ListMessageRequest());

foreach (var i in reply2.Data)
{
    Console.WriteLine("From " + i.Username + ": " + i.Message);
}

Console.WriteLine("Press any key to exit...");
Console.ReadKey();