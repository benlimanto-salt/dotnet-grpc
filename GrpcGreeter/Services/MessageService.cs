using Grpc.Core;
using GrpcGreeter;

namespace GrpcGreeter.Services;

public class MessageService : MessageSrv.MessageSrvBase
{
    private Google.Protobuf.Collections.RepeatedField<MsgRequest> _data = new Google.Protobuf.Collections.RepeatedField<MsgRequest>();
    private readonly ILogger<GreeterService> _logger;
    public MessageService(ILogger<GreeterService> logger, Data d)
    {
        _logger = logger;
        _data = d.data;
    }

    public override Task<MsgResponse> RecordMessage(MsgRequest request, ServerCallContext context)
    {
        this._data.Add(request);

        return Task.FromResult(new MsgResponse
        {
            Message = "Success " + request.Username
        });
    }

    public override Task<ListMessageResponse> ListMessage(ListMessageRequest request, ServerCallContext context)
    {
        // @see https://stackoverflow.com/a/63955917/4906348
        var resp = new ListMessageResponse();
        resp.Data.AddRange(_data);

        return Task.FromResult(resp);
    }
}
